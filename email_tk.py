def email_nova_fila(ticket,cidade,categoria,status,fila,abertura,titulo,aberto_por,descricao,impacto,fila_anterior,registros):
	return f'''
	<!DOCTYPE html>
	<html>
	<head>
	<style>
	table {{
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}}

	td, th {{
	  border: 1px solid #dddddd;
	  text-align: left;
	  padding: 8px;
	}}

	tr:nth-child(even) {{
	  background-color: #dddddd;
	}}
	</style>
	</head>
	<body>

	<h2>Olá, o ticket {ticket} da cidade de {cidade} mudou de Fila:</h2>

	<table class="redTable">
	<thead>
	<tr>
	</tr>
	</thead>
	<tfoot>
	<tr>
	</tr>
	</tfoot>
	<tbody>
	<tr>
	<td><b>TITULO</td>
	<td>{titulo}</td>
	</tr>
	<tr>
	<td><b>TICKET</td>
	<td>{ticket}</td>
	</tr>
	<tr>
	<td><b>CIDADE</td>
	<td>{cidade}</td>
	</tr>
	<tr>
	<td><b>CATEGORIA</td>
	<td>{categoria}</td>
	</tr>
	<tr>
	<td><b>STATUS</td>
	<td>{status}</td>
	</tr>
	<tr>
	<td><b>FILA ANTERIOR</td>
	<td><b>{fila_anterior}</td>
	</tr>
	<tr>
	<td><b>FILA ATUAL</td>
	<td><b>{fila}</td>
	</tr>
	<tr>
	<td><b>IMPACTO</td>
	<td>{impacto}</td>
	</tr>
	<tr>
	<td><b>ABERTO POR</td>
	<td>{aberto_por}</td>
	</tr>
	<tr>
	<td><b>ABERTO EM</td>
	<td>{abertura}</td>
	</tr>
	<tr>
	<td><b>DESCRI&Ccedil;&Atilde;O</td>
	<td>{descricao}</td>
	</tr>
	<tr>
	<td><b>link</td>
	<td><a href="https://newmonitor.virtua.com.br/user/datacenter/datacenter_view.php?idTicket={ticket}">{ticket}</a></td>
	</tr>
	</tbody>
	</table>
	{registros}
	</body>
	</html>'''

def email_novo_tk(ticket,cidade,categoria,status,fila,abertura,titulo,aberto_por,descricao,impacto,registros):
	return f'''
	<!DOCTYPE html>
	<html>
	<head>
	<style>
	table {{
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}}

	td, th {{
	  border: 1px solid #dddddd;
	  text-align: left;
	  padding: 8px;
	}}

	tr:nth-child(even) {{
	  background-color: #dddddd;
	}}
	</style>
	</head>
	<body>

	<h2>Olá, temos um novo ticket para cidade de {cidade}:</h2>

	<table class="redTable">
	<thead>
	<tr>
	</tr>
	</thead>
	<tfoot>
	<tr>
	</tr>
	</tfoot>
	<tbody>
	<tr>
	<td><b>TITULO</td>
	<td>{titulo}</td>
	</tr>
	<tr>
	<td><b>TICKET</td>
	<td>{ticket}</td>
	</tr>
	<tr>
	<td><b>CIDADE</td>
	<td>{cidade}</td>
	</tr>
	<tr>
	<td><b>CATEGORIA</td>
	<td>{categoria}</td>
	</tr>
	<tr>
	<td><b>STATUS</td>
	<td>{status}</td>
	</tr>
	<td><b>FILA ATUAL</td>
	<td>{fila}</td>
	</tr>
	<tr>
	<td><b>IMPACTO</td>
	<td>{impacto}</td>
	</tr>
	<tr>
	<td><b>ABERTO POR</td>
	<td>{aberto_por}</td>
	</tr>
	<tr>
	<td><b>ABERTO EM</td>
	<td>{abertura}</td>
	</tr>
	<tr>
	<td><b>DESCRI&Ccedil;&Atilde;O</td>
	<td>{descricao}</td>
	</tr>
	<tr>
	<td><b>link</td>
	<td><a href="https://newmonitor.virtua.com.br/user/datacenter/datacenter_view.php?idTicket={ticket}">{ticket}</a></td>
	</tr>
	</tbody>
	</table>
	{registros}
	</body>
	</html>'''


def email_fechamento_tk(ticket,cidade,categoria,abertura,titulo,aberto_por,descricao,impacto,status,registros):
	return f'''
	<!DOCTYPE html>
	<html>
	<head>
	<style>
	table {{
	  font-family: arial, sans-serif;
	  border-collapse: collapse;
	  width: 100%;
	}}

	td, th {{
	  border: 1px solid #dddddd;
	  text-align: left;
	  padding: 8px;
	}}

	tr:nth-child(even) {{
	  background-color: #dddddd;
	}}
	</style>
	</head>
	<body>

	<h2>Olá, o ticket {ticket} da cidade de {cidade} foi fechado:</h2>

	<table class="redTable">
	<thead>
	<tr>
	</tr>
	</thead>
	<tfoot>
	<tr>
	</tr>
	</tfoot>
	<tbody>
	<tr>
	<td><b>TITULO</td>
	<td>{titulo}</td>
	</tr>
	<tr>
	<td><b>TICKET</td>
	<td>{ticket}</td>
	</tr>
	<tr>
	<td><b>CIDADE</td>
	<td>{cidade}</td>
	</tr>
	<tr>
	<td><b>CATEGORIA</td>
	<td>{categoria}</td>
	</tr>
	<tr>
	<td><b>STATUS</td>
	<td>{status}</td>
	</tr>
	<tr>
	<td><b>IMPACTO</td>
	<td>{impacto}</td>
	</tr>
	<tr>
	<td><b>ABERTO POR</td>
	<td>{aberto_por}</td>
	</tr>
	<tr>
	<td><b>ABERTO EM</td>
	<td>{abertura}</td>
	</tr>
	<tr>
	<td><b>DESCRI&Ccedil;&Atilde;O</td>
	<td>{descricao}</td>
	</tr>
	<tr>
	<td><b>link</td>
	<td><a href="https://newmonitor.virtua.com.br/user/datacenter/datacenter_view.php?idTicket={ticket}">{ticket}</a></td>
	</tr>
	</tbody>
	</table>
	{registros}
	</body>
	</html>'''
