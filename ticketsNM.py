from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
from datetime import datetime, timedelta
from datetime import date
import pandas as pd
import sqlite3 as sql
import subprocess
import sys
import smtplib
from email.mime.text import MIMEText
import email_tk as m


smtp_ssl_host = 'smtp.gmail.com'
smtp_ssl_port = 465
username = '<email gmail>'
password = '<senha gmail>'
from_addr = username

path_chrome = "/<pasta do projeto>/chromedriver"
path_db = '/<pasta do projeto>/tickets.db'

login_nm ='<login newmonitor>'
senha_nm ='<senha newmonitor>'
url_ticket = '<url newmonitor>/user/datacenter/datacenter_view.php?idTicket='
url_login_nm = "<url newmonitor>/user/login_old.php"
url_tk_cidades = "<url newmonitor>/user/painel/painel_incidente_datacenter.php?idcidade=308,314,35,216,43,7,284,155,162,242,291,223,9,307,185,3,204,205,184,251,253&idfila="

chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

def addr_email(cidade):
    emails = {
        'Balneário Camboriu':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br'],
        'Biguaçu':['datacenter.fln@net.com.br'],
        'Blumenau':['datacenter.bnu@net.com.br','datacenter.fln@net.com.br'],
        'Brusque':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
        'Chapecó':['datacenter.cco@net.com.br','datacenter.fln@net.com.br'],
        'Criciúma':['datacenter.cua@net.com.br','datacenter.fln@net.com.br'],
        'Florianópolis':['datacenter.fln@net.com.br'],
        'Guaramirim':['datacenter.jve@net.com.br','datacenter.fln@net.com.br'],
        'Itajaí':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
        'Itapema':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
        'Jaragua do Sul':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
        'Joinville':['datacenter.jve@net.com.br','datacenter.fln@net.com.br'],
        'Lages':['datacenter.fln@net.com.br'],
        'Mafra':['datacenter.fln@net.com.br','datacenter.jve@net.com.br'],
        'Navegantes':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
        'Palhoça':['datacenter.fln@net.com.br'],
        'Sao Bento do Sul':['datacenter.fln@net.com.br','datacenter.jve@net.com.br'],
        'São José':['datacenter.fln@net.com.br'],
        'Videira':['datacenter.bnu@net.com.br','datacenter.iai@net.com.br','datacenter.fln@net.com.br'],
    }
    return emails.get(cidade, (['datacenter.fln@net.com.br']))

def verifica_novo(df, df2):
    for i, row in df.iterrows():
        if row['TICKET'] not in list(df2['TICKET']):
            print (f"Ticket novo: {row['TICKET']}")
            insert_tk = f"INSERT INTO tknewmonitor (ticket, cidade, hub, categoria, abertura, falha, status, fila)VALUES ({row['TICKET']}, \"{row['CIDADE']}\", \"{row['HUB']}\", \"{row['CATEGORIA']}\", \"{row['ABERTURA']}\", \"{row['FALHA']}\", \"{row['STATUS']}\", \"{row['FILA']}\");"
            cur.execute(insert_tk)
            conn.commit()
            mail_novo_tk(row)

def verifica_fechamento(df, df2):
    for i, row in df2.iterrows():
        if row['TICKET'] not in list(df['TICKET']):
            print (f"Ticket fechado: {row['TICKET']}")
            delete_tk = f"DELETE FROM tknewmonitor where ticket = {row['TICKET']};"
            cur.execute(delete_tk)
            conn.commit()
            mail_fechamento_tk(row)

def verifica_fila(df):
    for i, row in df.iterrows():
        find_tk = f"""SELECT * from tknewmonitor where ticket == {row['TICKET']};"""
        cur.execute(find_tk)
        linha = cur.fetchone()

        if linha is not None:
            if (row['FILA']==linha[8]):
                print (f"FILA {row['TICKET']} sem alteração")
            else:
                fila_anterior = linha[8]
                nova_fila = row['FILA']
                ticket = row['TICKET']
                mail_nova_fila(row,fila_anterior)
                update_tk = f"""UPDATE tknewmonitor set FILA ='{nova_fila}' where ticket = {ticket}"""
                print (update_tk)
                cur.execute(update_tk)
                conn.commit()
        else:
            print('nao ahei no BD')

def mail_novo_tk(novo):
    ticket = novo['TICKET']
    cidade = novo['CIDADE']
    categoria = novo['CATEGORIA']
    status = novo['STATUS']
    fila = novo['FILA']
    abertura = novo['ABERTURA']
    dados_tk = get_dados_tk(ticket)
    titulo = dados_tk[0]
    aberto_por = dados_tk[1]
    descricao = dados_tk[2]
    impacto = dados_tk[3]
    registros = dados_tk[5]

    to_addrs = addr_email(cidade)   

    message = MIMEText(m.email_novo_tk(ticket,cidade,categoria,status,fila,abertura,titulo,aberto_por,descricao,impacto,registros), 'html')
    
    message['subject'] = f'[{categoria}] Novo ticket: {ticket}'
    message['from'] = from_addr
    message['to'] = ', '.join(to_addrs)

    server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)

    server.login(username, password)
    server.sendmail(from_addr, to_addrs, message.as_string())
    server.quit()

def mail_fechamento_tk(fechado):
    ticket = fechado['TICKET']
    cidade = fechado['CIDADE']
    categoria = fechado['CATEGORIA']
    abertura = fechado['ABERTURA']
    dados_tk = get_dados_tk(ticket)
    titulo = dados_tk[0]
    aberto_por = dados_tk[1]
    descricao = dados_tk[2]
    impacto = dados_tk[3]
    status = dados_tk[4]
    registros = dados_tk[5]

    to_addrs = addr_email(cidade) 
    #to_addrs = ['william.desouza@claro.com.br']

    message = MIMEText(m.email_fechamento_tk(ticket,cidade,categoria,abertura,titulo,aberto_por,descricao,impacto,status,registros), 'html')
    
    message['subject'] = f'[{categoria}] Ticket: {ticket} FECHADO'
    message['from'] = from_addr
    message['to'] = ', '.join(to_addrs)

    server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)

    server.login(username, password)
    server.sendmail(from_addr, to_addrs, message.as_string())
    server.quit()

def mail_nova_fila(nova, fila_anterior):
    ticket = nova['TICKET']
    cidade = nova['CIDADE']
    categoria = nova['CATEGORIA']
    status = nova['STATUS']
    fila = nova['FILA']
    abertura = nova['ABERTURA']
    dados_tk = get_dados_tk(ticket)
    titulo = dados_tk[0]
    aberto_por = dados_tk[1]
    descricao = dados_tk[2]
    impacto = dados_tk[3]
    registros = dados_tk[5]

    to_addrs = to_addrs = addr_email(cidade)
    #to_addrs = ['william.desouza@claro.com.br']

    message = MIMEText(m.email_nova_fila(ticket,cidade,categoria,status,fila,abertura,titulo,aberto_por,descricao,impacto,fila_anterior,registros), 'html')


    message['subject'] = f'[{categoria}] ticket: {ticket} mudou de fila'
    message['from'] = from_addr
    message['to'] = ', '.join(to_addrs)

    server = smtplib.SMTP_SSL(smtp_ssl_host, smtp_ssl_port)
    server.login(username, password)
    server.sendmail(from_addr, to_addrs, message.as_string())
    server.quit()

def get_dados_tk(tk):
    browser.get(f"{url_ticket}{tk}")
    time.sleep(2)
    browser.save_screenshot('tk.png')
    titulo = browser.find_element_by_xpath("//*[@id='divTicket']/table/tbody/tr[9]/td[2]").get_attribute('textContent')
    aberto_por = browser.find_element_by_xpath("//*[@id='divInf']/table/tbody/tr[2]/td[2]").get_attribute('textContent')
    descricao = browser.find_element_by_xpath("//*[@id='divTicket']/table/tbody/tr[10]/td[2]").get_attribute('textContent')
    impacto = browser.find_element_by_xpath("//*[@id='divInf']/table/tbody/tr[4]/td[2]").get_attribute('textContent')
    status = browser.find_element_by_xpath("//*[@id='divTicket']/table/tbody/tr[11]/td[2]").get_attribute('textContent')
    tabela = browser.find_element_by_xpath('//*[@id="divMsg"]').get_attribute('outerHTML')
    time.sleep(1)
    df = pd.read_html(tabela)[0]
    pd.set_option('display.max_colwidth', -1)
    df = df.drop([1], axis=1)
    headers = df.iloc[0]
    new_df  = pd.DataFrame(df.values[1:], columns=headers)
    registros = new_df.to_html().replace("NaN", "#####")

    return titulo,aberto_por, descricao, impacto, status, registros


browser = webdriver.Chrome(chrome_options=chrome_options, executable_path=path_chrome)
browser.get(url_login_nm)
browser.find_element_by_xpath("//a[@href='#']").click()
browser.find_element_by_name('txtLogin').send_keys(login_nm)
browser.find_element_by_name('txtSenha').send_keys(senha_nm)
browser.find_element_by_xpath("//input[@type='button'][@value=' Entrar ']").click()
time.sleep(1)
browser.get(url_tk_cidades)
time.sleep(2)

df = pd.read_html(browser.page_source)[0]
df['ABERTURA'] = df['ABERTURA'].str.slice(0, 16, 1)
df['FALHA'] = df['FALHA'].str.slice(0, 16, 1)

conn = sql.connect(path_db)
cur = conn.cursor()
df2 = pd.read_sql('SELECT * FROM tknewmonitor', conn)

verifica_novo(df,df2)
verifica_fechamento(df,df2)
verifica_fila(df)

browser.quit()

